///////////////////////////////////////////////////////////////////////////////////////////////////////
//////////Falta funcion "desactivar" con interrupcion////////////////////////////////////////////////// 
//////////////////////////////////////////////////////////////////////////////////////////////////////

#include <Wire.h>
#include <Keypad.h>

boolean flag_inte = false;
int clave = 0;
int activada = 0;
const byte Filas = 4;  //Cuatro filas
const byte Cols = 4;   //Cuatro columnas
byte Pins_Filas[] = {11, 10, 9, 8};//Pines Arduino a las filasbyte 
byte Pins_Cols[] = {7, 6, 5, 4}; // Pines Arduino a las columnas.
//no utilizar los pines 1 y 0 para no interferir en Rx y Tx

char Teclas [ Filas ][ Cols ] =
 {
    {'1','2','3','A'},
    {'4','5','6','B'},
    {'7','8','9','C'},
    {'*','0','#','D'}
 };

char codigoSecreto[4] = {'1','4','1','8'}; // Aqui va el codigo secreto
// Para cambiar el tamaño de la clave, solo hay que cambiar el tamaño del array

int posicion=0;    // necesaria para la clave

Keypad Teclado1 = Keypad(makeKeymap(Teclas), Pins_Filas, Pins_Cols, Filas, Cols);
const int LEDPin = 13;        // pin para la sirena
const int PIRPin = 12;         // pin de entrada (for PIR sensor)
const int PIRPin1 = 3;         // pin de entrada (for PIR sensor)
int pirState = LOW;           // de inicio no hay movimiento
int pirState1 = LOW;           // de inicio no hay movimiento
int val = 0;                  // estado del pin
int val1 = 0;                  // estado del pin

void setup()
   {
      Serial.begin(9600) ;
      pinMode(LEDPin, OUTPUT); // pin para la sirena
      pinMode(2, OUTPUT); //pin 2 para led indicador
      pinMode(PIRPin, INPUT);
    
    }

void loop()
   {
    teclado();
    if (clave == 1 )
    {
       pir();
       pir1();
    }
   
   
      
 }

 void teclado()
 {
  char pulsacion = Teclado1.getKey() ; // leemos pulsacion
   
      if (pulsacion != 0) //Si el valor es 0 es que no se ha pulsado ninguna tecla
      {
               if (pulsacion == '#' || pulsacion == '*' && clave==0)
                        {
                           digitalWrite(2, HIGH); //pin 2 para led indicador
                           delay(10000); /////////////////////tiempo para activar y salir
                           clave = 1; // Activada
                           sirena_on();
                           delay(500);////////////////////////////////////////////////tiempo de sirena
                           sirena_of();
                           posicion = 0;
                           Serial.println("Activada");
                        }            
                  
                 
        
       
        }

       
        

  }

   void pir ()
{
  val = digitalRead(PIRPin);
   if (val == HIGH)   //si está activado
   { 
      
      if (pirState == LOW)  //si previamente estaba apagado
      {
        Serial.println("Sensor activado");
        delay(500);
        activada = 1;
        pirState = HIGH;
        sirena_on();
        delay(10000);////////////////////////////////////////////////tiempo de sirena
        sirena_of();
        
      }
   } 
   else   //si esta desactivado
   {
      digitalWrite(LEDPin, LOW); // LED OFF
      if (pirState == HIGH)  //si previamente estaba encendido
      {
        Serial.println("Sensor parado");
        pirState = LOW;
        activada = 0;
        
      }
   }
   
}

 void pir1 ()
{
  val1 = digitalRead(PIRPin1);
   if (val1 == HIGH)   //si está activado
   { 
      
      if (pirState1 == LOW)  //si previamente estaba apagado
      {
        Serial.println("Sensor activado1");
        delay(500);
        pirState1 = HIGH;
        sirena_on();
        delay(10000);////////////////////////////////////////////////tiempo de sirena
        sirena_of();
        
      }
   } 
   else   //si esta desactivado
   {
      
      if (pirState == HIGH)  //si previamente estaba encendido
      {
        Serial.println("Sensor1 parado");
        pirState1 = LOW;
        activada = 0;
        
      }
   }
   
}
void sirena_on()
{
  digitalWrite(13,HIGH);
  }
  void sirena_of()
{
  digitalWrite(13, LOW);
  
  }


//Creamos la interrupcion
attachInterrupt(0, int_alarma, HIGH);
}

//Codigo de la interrupcion
void int_alarma()
{
   flag_inte = true;
   while(flag_inte)
            {
            if (clave==1)
                        {  char pulsacion = Teclado1.getKey() ; // leemos pulsacion
                           Serial.println(pulsacion); // imprimimos pulsacion
                           cursor++;             // incrementamos el cursor
                           if (pulsacion == codigoSecreto[posicion])
                           posicion ++; // aumentamos posicion si es correcto el digito
            
                                          
                              if (posicion == 4)
                                 { // comprobamos que se han introducido los 4 correctamente
                                 
                                 delay(200);                                        
                                 clave=0; // desactivo
                                 posicion = 0;
                                 Serial.println("Desctivada");
                                 digitalWrite(2, LOW);
                                 flag_inte = false;
                  
                                 }  
                  
                           }
            }